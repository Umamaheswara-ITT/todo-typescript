
enum Status {
    'yet-to-start' = 'Yet to start',
    'in-progress' = 'In Progress',
    'completed' = 'Completed'
  }
  
  interface Task {
    id: number;
    subject: string;
    status: Status;
  }
  
  class ToDo {
    tasks: Task[] = [];
  
    public createTask(subject: string): void {
      const task = {
        id: this.getId(),
        subject,
        status: Status['yet-to-start'],
      } as Task;
      if (task.subject.length == 0){
        alert("Please enter the task!");
      }else{
        this.tasks.push(task);
        this.refresh();
      }
    }
  
    private getId(): number {
      const d = new Date();
      return d.getTime();
    }
  
    public changeTaskStatus(task: Task, status: Status, position: number): void {
      task.status = status;
  
      if (task.status === Status['completed']) {
        this.tasks.splice(position, 1);
      }
      this.refresh();
    }
  
    public refresh(): void {
      var container = document.getElementById('container') as HTMLInputElement;
      container.innerHTML = '';
      if (this.tasks.length === 0) {
        const elmNoTasks = document.createElement('h3');
        elmNoTasks.textContent = 'No Task Available!';
        elmNoTasks.style.textAlign = 'center';
        container.appendChild(elmNoTasks);
      }
      this.tasks.forEach((task, index) => {
        const elmSubject = document.createElement('div');
        elmSubject.className = 'subject';
        elmSubject.textContent = task.subject;
  
        const elmStatus = document.createElement('div')
        elmStatus.className = 'status';
        elmStatus.textContent = task.status;
  
        const elmBtnInProgress = document.createElement('button');
        elmBtnInProgress.className = 'action';
        elmBtnInProgress.textContent = 'In Progress';
        elmBtnInProgress.onclick = () => {
          this.changeTaskStatus(task, Status['in-progress'], index);
        }
  
        const elmBtnCompleted = document.createElement('button');
        elmBtnCompleted.className = 'action';
        elmBtnCompleted.textContent = 'Completed';
        elmBtnCompleted.onclick = () => {
          this.changeTaskStatus(task, Status['completed'], index);
        }
  
        const elmActionsContainer = document.createElement('div');
        elmActionsContainer.className = 'actions';
        elmActionsContainer.appendChild(elmBtnInProgress);
        elmActionsContainer.appendChild(elmBtnCompleted);
  
        const elmContainer = document.createElement('div');
        elmContainer.className = 'task';
        elmContainer.appendChild(elmSubject);
        elmContainer.appendChild(elmStatus);
        elmContainer.appendChild(elmActionsContainer);
        
        container.appendChild(elmContainer);
      });
    }
  }
  
  window.addEventListener('load', () => {
    const todo = new ToDo();
    todo.createTask('Bike Riding');
    todo.createTask('Breakfast');
    todo.createTask('Meeting with CEO');
    const addButton = document.getElementById('add') as HTMLInputElement;
    addButton.addEventListener('click', () => {
      var container = document.getElementById('container');
      let elm = document.getElementById('subject') as HTMLInputElement;
      todo.createTask(elm.value);
      elm.value = "";
    });
  });
  